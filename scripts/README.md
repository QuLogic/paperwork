# Script examples

Scripts related to Paperwork.


## export\_all\_to\_pdf.sh

Convert all your Paperwork documents into PDF. Takes your currently-active
work directory as source. Takes a destination directory as argument.

[script](export_all_to_pdf.sh)


## convert\_jpeg\_to\_png.sh

Starting with Paperwork 2.2, Paperwork stores the scanned images as PNG
instead of JPEG. Existing documents with JPEG images are not converted
(Paperwork 2.2 still read them fine).

You can use this script to convert the images if you want. There is no gain
to do it.

[script](convert_jpeg_to_png.sh)
