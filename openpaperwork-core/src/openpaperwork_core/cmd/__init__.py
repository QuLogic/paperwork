class DummyConsole:
    def print(self, *args, **kwargs):
        pass

    def input(self, prompt=""):
        return None
