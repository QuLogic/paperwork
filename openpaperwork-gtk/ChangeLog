2024/08/28 - 2.2.5:
- No changes

2024/08/27 - 2.2.4:
- No changes

2024/05/12 - 2.2.3:
- Scan drawer: handle gracefully cases when the scan height is not known
  from the start.

2024/02/13 - 2.2.2:
- dependencies: Make really sure python3-gi-cairo is installed

2023/09/17 - 2.2.1:
- Add build-system.build-backend to pyproject.toml

2023/09/16 - 2.2.0:
- setup.py has been replaced by pyproject.toml
- fs.gio.fs_get_mtime(): do not trust Gio.File.query_info(TIME_CHANGED) on
  Windows. Prefer os.stat()

2023/01/08 - 2.1.2:
- dependencies: fix: depends on openpaperwork-core
- autoscrolling: take into account that Gdk.Cursor.new_for_display() may fail
  on some systems (wayland-only systems)
- Core-GTK: Add a plugin showing a dialog when the app is closed and any
  running background task is still running. Hopefully, it will help for issue
  #1034 ("sqlite: database is locked")

2022/01/31 - 2.1.1:
- Fix: take into account that Cursor.new_for_display() may fail with Wayland

2021/12/05 - 2.1.0:
- Bug report: Clarify the use of the author field

2021/05/24 - 2.0.3:
- Swedish translations added
- Add LICENSE file in pypi package

2021/01/01 - 2.0.2:
- No changes

2020/11/15 - 2.0.1:
- fs.gio: Text files must be encoded in UTF-8
- Include tests in Pypi package (thanks to Elliott Sales de Andrade)

2020/10/17 - 2.0:
- Initial release
